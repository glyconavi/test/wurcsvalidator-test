package org.glyconavi.WURCSVALIDATOR.cli;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.HashMap;
import org.glycoinfo.WURCSFramework.util.validation.WURCSValidator;
import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        String sequence = "";
        String result = "";
        if (args.length == 2) {
            sequence = args[1];
        }

        try {
            result = wurcsValidator(sequence);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally{
            System.out.println(result);
        }
    }

    private static String wurcsValidator(String wurcs) {
        //Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        WURCSValidator validator = new WURCSValidator();
        validator.start(wurcs);
        //return gson.toJson(validator.getReport());
        String str_out = "";
        str_out = "Results: " + validator.getReport().getResults();
        str_out += "Input WURCS:\t" + wurcs + "\n";
        str_out += "Normalized:\t" + validator.getReport().getStandardString();
        return str_out;
    }
}
