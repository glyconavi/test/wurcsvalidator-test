# WURCSVALIDATOR-TEST

## Setup

1. 
```
git clone https://gitlab.com/glyconavi/test/wurcsvalidator-test.git
```

2. 
```
cd wurcsvalidator-test/
```

3. 
```
mvn clean compile assembly:single
```

## Usage

```
java -jar ./target/WURCSVALIDATOR-TEST.jar -s WURCS=2.0/3,4,3/[a2122h-1b_1-5][a2112h-1b_1-5][a2112h-1b_1-5_2*NCC/3=O]/1-2-3-2/a4-b1_b4-c1_c3-d1
```

# Result

```
12:03:40.258 [main] DEBUG org.glycoinfo.WURCSFramework.util.array.WURCSImporter - Start extracting WURCSArray from WURCS=2.0/4,4,3/[u2122A][a2122h-1x_1-5][a211h-1x_1-4][a211h-1x_1-?]/1-2-3-4/a?-b1_a?-c1_a?-d1
12:03:40.274 [main] DEBUG org.glycoinfo.WURCSFramework.util.graph.WURCSGraphNormalizer - Reverse: 0
12:03:40.274 [main] DEBUG org.glycoinfo.WURCSFramework.util.graph.WURCSGraphNormalizer - Reverse: 1
...
...
12:03:40.293 [main] DEBUG org.glycoinfo.WURCSFramework.util.exchange.WURCSGraphToArray - candidate UniqueRES: a2122h-1x_1-5
12:03:40.293 [main] DEBUG org.glycoinfo.WURCSFramework.util.exchange.WURCSGraphToArray - candidate UniqueRES: a211h-1x_1-4
12:03:40.293 [main] DEBUG org.glycoinfo.WURCSFramework.util.exchange.WURCSGraphToArray - candidate UniqueRES: a211h-1x_1-?
Results: Outputed strings:
WURCS=2.0/4,4,3/[u2122A][a2122h-1x_1-5][a211h-1x_1-4][a211h-1x_1-?]/1-2-3-4/a?-b1_a?-c1_a?-d1
Input WURCS:	WURCS=2.0/4,4,3/[u2122A][a2122h-1x_1-5][a211h-1x_1-4][a211h-1x_1-?]/1-2-3-4/a?-b1_a?-c1_a?-d1
Normalized:	WURCS=2.0/4,4,3/[u2122A][a2122h-1x_1-5][a211h-1x_1-4][a211h-1x_1-?]/1-2-3-4/a?-b1_a?-c1_a?-d1
```
